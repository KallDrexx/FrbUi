FrbUi
=====

UI and Layouting system for the FlatRedBall XNA engine.  It is meant to make it quick and easy to lay out complicated user interfaces in a programmer friendly way.

For documentation, see the [wiki](https://github.com/KallDrexx/FrbUi/wiki).
